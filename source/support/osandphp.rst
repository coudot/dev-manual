Distribution and PHP support Policy
===================================

Distribution OSes have different interpretations of what a 'supported version' is, here are the OS and PHP versions FusionDirectory support.

Server OSes 
-----------

 * Debian: stable and oldstable
 * Ubuntu: the two latest LTS releases
 * Enterprise Linux (RHEL, CentOS, ...): the latest major release


PHP versions
============

The version of PHP depend on the FusionDirectory version.

Fusiondirectory need at least PHP 5.6.

 * Fusiondirectory 1.3 need PHP 5.6 
 * Fusiondirectory 1.4 need PHP 7.0

