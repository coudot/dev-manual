Translate FusionDirectory
=========================

We are using `Transifex <https://www.transifex.com/fusiondirectory/FusionDirectory-1x/>`_

* Create a transifex account
* Ask to be added to the language group you want to translate
* Start translating
