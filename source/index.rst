.. FusionDirectory development documentation master file, created by
   sphinx-quickstart on Mon Aug 28 16:27:31 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FusionDirectory development's documentation!
=======================================================

.. image:: /_static/images/fd_logo.png
   :alt: FusionDirectory
   :align: center


Contents:

.. toctree::
   :maxdepth: 2

   contribute/guidelines.rst
   coding/index
   pluginsystem/index
   api/index
   schemas/index
   themes/index
   translate/index
   release/index
   support/index
   license/index
   code-of-conduct.rst

.. include:: globals.rst


